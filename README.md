#Badass List of Tricks

### Making of
* [The Front-end of Middle-earth](http://www.html5rocks.com/en/tutorials/casestudies/hobbit-front-end/):"The Hobbit" Making-of

###Monitoring
* [Monitoring 60fps](http://engineering.wingify.com/posts/getting-60fps-using-devtools/):Getting 60 FPS using Chrome devtools

###Ease
* [Understanding Penner Equations](https://www.kirupa.com/html5/animating_with_easing_functions_in_javascript.htm)
* [Smoothing equations](http://blog.neave.com/post/69482887767/smoothing-equations-a-simpler-way-to-ease-variables): Smoothing equations: a simpler way to ease variables
* [stylie](http://jeremyckahn.github.io/stylie/):A fun CSS animation tool like Ceaser

### Animation
* [Image Sequence methods](http://awardwinningfjords.com/2012/03/08/image-sequences.html): Image Sequences
* [High Performance Animation](http://www.html5rocks.com/en/tutorials/speed/high-performance-animations/?redirect_from_locale=fr):
* [CSS Animations Tricks](http://css-tricks.com/css-animation-tricks/)
* [Why Moving Elements With Translate() Is Better Than Pos:abs Top/left](http://www.paulirish.com/2012/why-moving-elements-with-translate-is-better-than-posabs-topleft/)
* [Accelerated Rendering in Chrome](http://www.html5rocks.com/en/tutorials/speed/layers/)
* [High Performance Animations](http://www.html5rocks.com/en/tutorials/speed/high-performance-animations/?redirect_from_locale=fr)
* [HTML5 ANIMATION By Bartek](http://bartekdrozdz.com/blog/html5-animation.html)
* [How to kill GSAP tweens](http://forums.greensock.com/topic/8917-all-the-methods-to-kill-a-tween/)
* [Scrolling Performance](http://www.html5rocks.com/en/tutorials/speed/scrolling/)
* [CSS animations and transitions performance: looking inside the browser](http://blogs.adobe.com/webplatform/2014/03/18/css-animations-and-transitions-performance/)
* [cancelRequestAnimationFrame() For Paul Irish requestAnimationFrame()](http://notes.jetienne.com/2011/05/18/cancelRequestAnimFrame-for-paul-irish-requestAnimFrame.html)
* [How to Use steps() in CSS Animations](http://designmodo.com/steps-css-animations/)
* [Understanding CSS Timing Functions](http://www.smashingmagazine.com/2014/04/15/understanding-css-timing-functions/)
* [Giving Animations Life](https://medium.com/tictail-makers/giving-animations-life-8b20165224c5)
* [CSS Parallax](http://blog.keithclark.co.uk/pure-css-parallax-websites/)
* [A guide for SVG Animation](http://css-tricks.com/guide-svg-animations-smil/)

### Scroll
* [Tackling inertia scroll](http://blog.danyll.com/lethargy-tackling-inertial-scroll/)
* [Page Scroll Effects](http://codyhouse.co/gem/page-scroll-effects/)

### Templating
* [Templating for invoice](http://bl.ocks.org/biovisualize/7904516): Invoice using a minimalist d3 templating system

### Fonts
* [Optimizing Web Font Rendering Performance](http://www.igvita.com/2014/01/31/optimizing-web-font-rendering-performance/)

### Mail
* [The Ultimate Guide to CSS](http://www.campaignmonitor.com/css/): A complete breakdown of the CSS support for every popular mobile, web and desktop email client on the planet.

### Mobile
* [300ms tap delay, gone away](http://updates.html5rocks.com/2013/12/300ms-tap-delay-gone-away):300ms tap delay, gone away
* [Chrome DevTools for Mobile: Screencast and Emulation](http://www.html5rocks.com/en/tutorials/developertools/mobile/?redirect_from_locale=fr):Developing for mobile should be just as easy as it is developing for desktop.

### HTML5 API
* [Fullscreen API](http://generatedcontent.org/post/70347573294/is-your-fullscreen-api-code-up-to-date-find-out-how-to):Is your Fullscreen API code up to date? Find out how to make it work the same in modern browsers

### SASS/CSS/LESS
* [BEM Syntax](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
* [CSS Styleguide](http://cssguidelin.es/)

### JS
* [jQuery to VanillaJS](http://putaindecode.fr/posts/js/de-jquery-a-vanillajs/)
* [You Might Not Need Jquery](http://youmightnotneedjquery.com/)
* [jQuery Coding Standards and Best Practices](http://lab.abhinayrathore.com/jquery-standards/)
* [Seven JavaScript Quirks I Wish I’d Known About](http://developer.telerik.com/featured/seven-javascript-quirks-i-wish-id-known-about/)

### Grunt
* [GRUNT FOR PERFORMANCE OPTIMIZATION](http://yeoman.io/blog/performance-optimization.html)

### CMS / CONTENT
* [JSON EDITOR](https://github.com/jdorn/json-editor)


### DEV TOOLS
* [Chrome Cheatsheet](http://anti-code.com/devtools-cheatsheet/)
* [Debugging Asynchronous JavaScript with Chrome DevTools](http://www.html5rocks.com/en/tutorials/developertools/async-call-stack/)

### LARAVEL
* [Laravel Recipes](http://laravel-recipes.com/)

### TIPS
* [Prevent automatic browser scroll on refresh](https://stackoverflow.com/questions/7035331/prevent-automatic-browser-scroll-on-refresh/18633915#18633915)
* [Front end Developer Tips](http://benfrain.com/top-tips-selection-unrelated-front-end-developer-tips/)


#REFERENCES
* [http://ds5.citroen.com.br/](http://ds5.citroen.com.br/)
* [http://toyotahalloffame.com](http://toyotahalloffame.com/history)
* [http://ondo.tv/1st/](http://ondo.tv/1st/)